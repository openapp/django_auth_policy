import datetime
import re
import unicodedata

from django.contrib.auth.hashers import check_password
from django.core.exceptions import ValidationError
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from django_auth_policy import BasePolicy, models


def _normalize_unicode(value):
    try:
        value = unicodedata.normalize('NFKD', unicode(value))
        return value.encode('ascii', 'ignore').strip().lower()
    except UnicodeDecodeError:
        return value


class PasswordStrengthPolicy(BasePolicy):
    """ Password strength policy classes must implement:

    `validate` a method which accept a password and the related user and raises
    a validation error when the password doesn't validate the policy.

    Optionally:

    `policy_text` a property which returns a short text to be displayed in
    password policy explenations

    `policy_caption` a property which returns a short caption to be displayed
    with the password policy.
    """
    show_policy = True

    def validate(self, value, user=None, generated_password=False):
        raise NotImplemented()

    @property
    def policy_text(self):
        return None

    @property
    def policy_caption(self):
        return None


class PasswordMinLength(PasswordStrengthPolicy):
    min_length = 10
    text = _('Passwords must be at least {min_length} characters in length.')

    def __init__(self, **kwargs):
        if 'min_length' in kwargs:
            self.min_length = kwargs.pop('min_length')
        super(PasswordMinLength, self).__init__(**kwargs)

    def validate(self, value, user=None, generated_password=False):
        if self.min_length is None:
            return

        if len(value) < self.min_length:
            msg = self.text.format(min_length=self.min_length)
            raise ValidationError(msg, code='password_min_length')

    @property
    def policy_text(self):
        return self.text.format(min_length=self.min_length)


class PasswordContains(PasswordStrengthPolicy):
    """ Base class which validates if passwords contain at least a certain
    number of characters from a certain set.
    """
    chars = None
    min_count = 1
    text = None
    plural_text = None

    def validate(self, value, user=None, generated_password=False):
        pw_set = set(value)
        if len(pw_set.intersection(self.chars)) < self.min_count:
            raise ValidationError(self.text, 'password_complexity')

    @property
    def policy_text(self):
        if self.min_count > 1:
            return self.plural_text.format(min_count=self.min_count)
        else:
            return self.text.format(min_count=self.min_count)

    @property
    def policy_caption(self):
        return self.chars


class PasswordContainsUpperCase(PasswordContains):
    chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    text = _('Passwords must have at least one uppercase character.')
    plural_text = _('Passwords must have at least {min_count} '
                    'uppercase characters.')


class PasswordContainsLowerCase(PasswordContains):
    chars = 'abcdefghijklmnopqrstuvwxyz'
    text = _('Passwords must have at least one lowercase character.')
    plural_text = _('Passwords must have at least {min_count} '
                    'lowercase characters.')


class PasswordContainsNumbers(PasswordContains):
    chars = '0123456789'
    text = _('Passwords must have at least one number.')
    plural_text = _('Passwords must have at least {min_count} '
                    'numbers.')


class PasswordContainsSymbols(PasswordContains):
    chars = '!@#$%^&*()_+-={}[]:;"\'|\\,.<>?/~` '
    text = _(
        'Passwords must have at least one special character (punctuation).'
    )
    plural_text = _('Passwords must have at least {min_count} special '
                    'characters (punctuation).')


class PasswordContainsAlphabetics(PasswordContains):
    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    text = _('Passwords must have at least one alphabetic character.')
    plural_text = _('Passwords must have at least {min_count} '
                    'alphabetic characters.')


class PasswordUserAttrs(PasswordStrengthPolicy):
    """ Validate if password doesn't contain values from a list of user
    attributes. Every attribute will be normalized into ascii and split
    on non alphanumerics.

    Use this in the clean method of password forms

    `value`: password
    `user`: user object with attributes

    Example, which would raise a ValidationError:

        user.first_name = 'John'
        password_user_attrs('johns_password', user)
    """
    user_attrs = ('email', 'first_name', 'last_name', 'username')
    text = _('Passwords are not allowed to contain (pieces of) your name '
             'or email.')

    _non_alphanum = re.compile(r'[^a-z0-9]')

    def validate(self, value, user=None, generated_password=False):
        if user is None:
            return

        simple_pass = _normalize_unicode(value)
        for attr in self.user_attrs:
            v = getattr(user, attr, None)
            if not attr or len(attr) < 4:
                continue

            v = _normalize_unicode(v)

            for piece in self._non_alphanum.split(v):
                if len(piece) < 4:
                    continue

                if piece in simple_pass:
                    raise ValidationError(self.text, 'password_user_attrs')

    @property
    def policy_text(self):
        return self.text.format()


class PasswordDisallowedTerms(PasswordStrengthPolicy):
    """ Disallow a (short) list of terms in passwords
    Ideal for too obvious terms like the name of the site or company
    """
    terms = None
    text = _('Passwords are not allowed to contain the following term(s): '
             '{terms}.')
    show_policy = False

    def __init__(self, **kwargs):
        terms = kwargs.pop('terms')
        self.terms = [_normalize_unicode(term) for term in terms]

        super(PasswordDisallowedTerms, self).__init__(**kwargs)

    def validate(self, value, user=None, generated_password=False):
        simple_pass = _normalize_unicode(value)
        found = []
        for term in self.terms:
            if term in simple_pass:
                found.append(term)

        if found:
            msg = self.text.format(terms=u', '.join(found))
            raise ValidationError(msg, 'password_disallowed_terms')

    @property
    def policy_text(self):
        return self.text.format(terms=u', '.join(self.terms))


class PasswordCheckHistory(PasswordStrengthPolicy):
    """ Checks if the password has been used before """
    history_length = 10
    text = (
        'You can not reuse a password that you\'ve already '
        'used in the last 10 times'
    )

    def validate(self, value, user=None, generated_password=False):
        password_history = models.PasswordChange.objects.filter(user=user)\
            .order_by('-timestamp')

        if len(password_history) > self.history_length:
            password_history = password_history[:self.history_length]
        if any(check_password(value, ph.password)
               for ph in password_history):
            raise ValidationError(
                self.text.format(
                    history_length=self.history_length
                ),
                code='password_check_history'
            )

    @property
    def policy_text(self):
        return self.text.format(history_length=self.history_length)


class PasswordContainsUnicodeCharacters(PasswordStrengthPolicy):
    text = ('Passwords must include a unicode character that is not a letter')

    def validate(self, value, user=None, generated_password=False):
        found = False
        for char in value:
            if char.isalpha() and not char.isupper() and not char.islower():
                found = True

        if not found:
            raise ValidationError(
                self.text,
                code='password_contains_unicode_characters'
            )

    @property
    def policy_text(self):
        return self.text


class PasswordChangeMinAge(PasswordStrengthPolicy):
    """ Enforces expired password to be changed.
    """
    # Password expiration period in days
    min_age = 1 * 24 * 60 * 60
    text = (
        'You will be unable to change your password '
        'if it has been changed in the last 24 hours'
    )
    allow_empty_password_history = False

    def validate(self, value, user=None, generated_password=False):
        """ If passwords are generated (temporary passwords),
        we ignore this policy as this doesn't make sense.

        It will throw errors when you are trying to change the password
        for the second time if you have a scenario like this:

        ---x------------------x-------------------------x-->
           User changes   Password is reset    User changes
           password       by admin             password again

        If this happen within 24 hours, this throw an error and the user
        won't be able to change his password.
        """
        password_history = models.PasswordChange.objects.filter(
            user=user,
            successful=True
        ).order_by('-timestamp')

        if not password_history or generated_password:
            return


        cant_change_until = (
            password_history.first().timestamp +
            datetime.timedelta(seconds=self.min_age)
        )
        last_password = password_history.first()
        if not last_password.is_temporary and timezone.now() < cant_change_until:
            raise ValidationError(self.text, code='password-min-age')

    @property
    def policy_text(self):
        return self.text
