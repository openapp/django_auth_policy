from django.utils.module_loading import import_by_path
from django.core.exceptions import ValidationError


class Manager(object):
    number_correct_policies = 3
    _policies = []
    policy_texts = []
    show_policy = True

    @property
    def policy_text(self):
        return (
            'Please comply to at least '
            '{number_correct_policies} of the following:'
        ).format(number_correct_policies=self.number_correct_policies)

    def __init__(self, *args):
        for policy_path, kwargs in args:
            policy_class = import_by_path(policy_path)
            policy = policy_class(**kwargs)
            self._policies.append(policy)

            if policy.show_policy and policy.policy_text:
                self.policy_texts.append({
                    'text': policy.policy_text,
                    'caption': policy.policy_caption,
                })

        assert self.number_correct_policies <= len(self._policies), (
            'You need to use more then {number_correct_policies} policies '
            'in this manager'
        )

    def validate(self, password, user, generated_password=False):
        valid_policies = len(self._policies)
        for policy in self._policies:
            try:
                policy.validate(password, user, generated_password)
            except ValidationError:
                valid_policies -= 1

        if valid_policies < self.number_correct_policies:
            raise ValidationError(
                'You need to comply to at least 3 policies',
            )
