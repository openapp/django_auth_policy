import logging
import datetime

from django import http
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import logout
from django.core.cache import get_cache
from django.core.urlresolvers import resolve, reverse
from django.utils import timezone
from django.utils.importlib import import_module
from django.views.decorators.csrf import requires_csrf_token

from django_auth_policy import models
from django_auth_policy.handlers import PasswordChangePolicyHandler
from django_auth_policy.forms import StrictPasswordChangeForm


logger = logging.getLogger(__name__)


class AuthenticationPolicyMiddleware(object):
    """ This middleware enforces the following policy:
    - Change of password when password has expired;
    - Change of password when user has a temporary password;
    - Logout disabled users;

    This is enforced using middleware to prevent users from accessing any page
    handled by Django without the policy being enforced.
    """

    change_password_path = reverse(getattr(
        settings, 'ENFORCED_PASSWORD_CHANGE_VIEW_NAME', 'password_change'))
    login_path = reverse(getattr(settings, 'LOGIN_VIEW_NAME', 'login'))
    logout_path = reverse(getattr(settings, 'LOGOUT_VIEW_NAME', 'logout'))

    password_change_policy_handler = PasswordChangePolicyHandler()

    def process_request(self, request):
        assert hasattr(request, 'user'), (
            'AuthenticationPolicyMiddleware needs a user attribute on '
            'request, add AuthenticationMiddleware before '
            'AuthenticationPolicyMiddleware in MIDDLEWARE_CLASSES')

        if not request.user.is_authenticated():
            return None

        # Log out disabled users
        if not request.user.is_active:
            logger.info('Log out inactive user, user=%s', request.user)
            view_func, args, kwargs = resolve(self.logout_path)
            return view_func(request, *args, **kwargs)

        if request.user.is_superuser:
            return None

        # Do not do password change for certain URLs
        if request.path in (self.change_password_path, self.login_path,
                            self.logout_path):
            return None

        # Check for 'enforce_password_change' in session set by login view
        # We need an exception if the user is logging out
        current_url = request.path
        logout_url = reverse('logout')
        if current_url in logout_url:
            return None
        if request.session.get('password_change_enforce', False):
            return self.password_change(request)

        return None

    def process_response(self, request, response):
        if not hasattr(request, 'user') or not request.user.is_authenticated():
            return response

        days_before_expiring = getattr(
            settings,
            'DAYS_BEFORE_WARNING_USERS_ABOUT_EXPIRED_PASSWORDS',
            0
        )
        if days_before_expiring > 0:
            if reverse('login') in request.path:
                user = request.user

                last_password = models.PasswordChange.objects.filter(
                    user=user,
                    successful=True
                ).order_by('-timestamp').first()

                max_age = get_policy_parameters(
                    'PASSWORD_CHANGE_POLICIES',
                    'django_auth_policy.password_change.PasswordChangeExpired',
                    'max_age'
                )
                if last_password:
                    start_warning_date = (
                        last_password.timestamp
                        + datetime.timedelta(seconds=max_age)
                        - datetime.timedelta(days=days_before_expiring)
                    )
                    expires_on = (
                        last_password.timestamp
                        + datetime.timedelta(seconds=max_age)
                    )
                    number_days = abs(timezone.now() - expires_on).days
                    if start_warning_date <= timezone.now() < expires_on:
                        message = (
                            'Your password will expire in '
                            '{number_days} {day}, please change it'
                        ).format(day='days' if number_days > 1 else 'day',
                                 number_days=number_days)
                        messages.warning(request, message)
        # When password change is enforced, check if this is still required
        # for next request
        self.password_change_policy_handler.update_session(
            request, request.user)

        return response

    def password_change(self, request):
        """ Return 'password_change' view.
        This resolves the view with the name 'password_change'.

        Overwrite this method when needed.
        """
        view_func, args, kwargs = resolve(self.change_password_path)

        if 'password_change_form' in kwargs:
            assert issubclass(kwargs['password_change_form'],
                              StrictPasswordChangeForm), (
                "Use django_auth_policy StrictPasswordChangeForm for password "
                "changes.")

        # Provide extra context to be used in the password_change template
        if 'extra_context' in kwargs:
            kwargs['extra_context']['password_change_enforce'] = \
                request.session.get('password_change_enforce')
            kwargs['extra_context']['password_change_enforce_msg'] = \
                request.session.get('password_change_enforce_msg')

        # Run 'requires_csrf_token' because CSRF middleware might have been
        # skipped over here
        return requires_csrf_token(view_func)(request, *args, **kwargs)


class LoginRequiredMiddleware(object):
    """ Middleware which enforces authentication for all requests.
    """
    login_path = reverse(getattr(settings, 'LOGIN_VIEW_NAME', 'login'))
    logout_path = reverse(getattr(settings, 'LOGOUT_VIEW_NAME', 'logout'))
    public_urls = getattr(settings, 'PUBLIC_URLS', [])
    public_urls.append(login_path)
    public_urls.append(logout_path)

    def process_request(self, request):
        if not hasattr(request, 'user'):
            raise Exception('Install Authentication middleware before '
                            'LoginRequiredMiddleware')

        if request.user.is_authenticated():
            return None

        # Do not require authentication for certain URLs
        if request.path in self.public_urls:
            return None

        # Django should not serve STATIC files in production, but for
        # DEBUG mode this should be no problem (development)
        if(settings.STATIC_URL
           and request.path.startswith(settings.STATIC_URL)):

            if settings.DEBUG:
                return None
            else:
                return http.HttpResponse('Unauthenticated', status=401)

        # When serving MEDIA files through Django we will not display a login
        # form, but instead return HTTP 401, but for DEBUG mode this should be
        # no problem (development)
        if(settings.MEDIA_URL
           and request.path.startswith(settings.MEDIA_URL)):

            if settings.DEBUG:
                return None
            else:
                return http.HttpResponse('Unauthenticated', status=401)

        # Ajax views should not display a login form, we use HTTP 401 to
        # indicate an unauthorized request, like a session timeout
        if request.is_ajax():
            return http.HttpResponse('Unauthenticated', status=401)

        view_func, args, kwargs = resolve(self.login_path)
        return requires_csrf_token(view_func)(request, *args, **kwargs)


class TimeOutMiddleware(object):
    default_timeout = 20 * 60 * 60

    def process_request(self, request):
        if request.user.is_authenticated():
            if 'last_request' in request.session:
                elapsedTime = (
                    datetime.datetime.now()
                    - datetime.datetime.strptime(
                        request.session['last_request'],
                        '%x %X'
                    )
                )
                print elapsedTime
                timeout = getattr(settings, 'TIMEOUT', self.default_timeout)
                if elapsedTime.seconds > timeout:
                    del request.session['last_request']
                    logout(request)

            request.session['last_request'] = (
                datetime.datetime.now().strftime('%x %X')
            )
        else:
            if 'last_request' in request.session:
                del request.session['last_request']
        return None


class UserRestrictMiddleware(object):
    def process_request(self, request):
        """
        Checks if different session exists for user and deletes it.
        """
        if request.user.is_authenticated():
            cache = get_cache('default')
            cache_timeout = 86400
            cache_key = "user_pk_%s_restrict" % request.user.pk
            cache_value = cache.get(cache_key)

            if cache_value is not None:
                if request.session.session_key != cache_value:
                    engine = import_module(settings.SESSION_ENGINE)
                    session = engine.SessionStore(session_key=cache_value)
                    session.delete()
                    cache.set(cache_key, request.session.session_key,
                              cache_timeout)
            else:
                cache.set(
                    cache_key,
                    request.session.session_key,
                    cache_timeout
                )


def get_policy_parameters(policy_category, policy_name, parameter):
    for policy in getattr(settings, policy_category):
        if parameter in policy[1]:
            return policy[1][parameter]
