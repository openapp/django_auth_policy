IDEAS / TODOS
=============

* Optionally keep a limited history of passwords (hashed) to prevent password
  reuse, password model field has been added.

* Expand docs

* Add example on how to implement Djangos password reset views

* Add example on how to send temporary passwords by email

* Add tests for Django admin code?

* Add tests for UserChange tracking

* Add more signals as hooks for additional functionality

* Optionally prevent usage of passwords from a list of common passwords
  
  Supply a publicly available list with django_auth_policy?

  Filter this list based on the other policy rules.

* Optionally use different source of random data for temporary password
  generation

* Allow projects to use customized temporary password generator

* Allow temporary passwords to expire?

* Optionally check password complexity at login and enforce new password
  for weak passwords

* Optionally measure entropy of passwords
  Research: any good enough algorithms available?

* Add password strength ideas from OWASP:
  https://www.owasp.org/index.php/Authentication_Cheat_Sheet#Implement_Proper_Password_Strength_Controls

  - 3 out of 4 complexity rules
  - not more than 2 identical characters in a row (ie. 111 not allowed)

* Add authentication ideas from Mozilla:
  https://wiki.mozilla.org/WebAppSec/Secure_Coding_Guidelines#Authentication

* Add password hasher for bcrypt/scrypt/PDKDF2 which stores nonce/salt
  seperately (not in the database) but still accessible by all web processes
  Also pepper the hash with Django SECRET_KEY + some fixed string

* Use sensitive_variables and sensitive_post_parameters to hide passwords from
  Django error reports

* Only login from whitelisted IP addresses/ranges.
  Or provide example which limits admin/superuser login
  to certain IP addresses by sending the REMOTE_ADDR to authentication backend.

* Only allow certain usernames at login (list of possible regex-es)
  Could be used to enforce usernames to end with company domainname.

* Add password strength checker which uses unicodedata.category
  http://www.unicode.org/reports/tr44/tr44-4.html#General_Category_Values

* Optionally forbid/logout users when certain headers change (USER_AGENT,
  REMOTE_ADDR, X_FORWARDED_FOR) Also log this.

* Optionally store hash of password in session and logoff user when hash changes
  (i.e. user changed password)

* Implement a login_required_exempt decorator
  (which sets an attribute on the specified view)

* LoginRequiredMiddleware could catch 404's and return login page to avoud
  exposing which URLs are available on a site (because it returns 404
  or login screens depending of existence of URL resource)

* Allow regex expressions for LoginRequiredMiddleware

Backward incompatible changes for Django Auth Policy 0.10:

* Store authentication backend in attempt, i.e.:

	user = authenticate(username=u, password=p)
	if user is not None:
		attempt.backend = user.backend

* Move forms into documentation examples and tests?
  Create a function that can be called within an form clean method.
  This stimulates projects to implement Django Auth Policy correctly in project
  specific forms.

* pre_auth_checks: Change this function and underlying policies to accept a
  `credentials` dictionary and a HTTP `headers` dictionary.

* Authentication policy handler method `post_auth_checks` should not run
  `auth_success`

* Store all HTTP headers JSON encoded with login attempt, in addition to
  current fields

* Store a representation of the user with every attempt, in addtion to FG to
  user. Make FK field nullable and set on_delete to NULL the field.

Far future ideas:

* Remove Django dependency from main code and add special module/package for
  Django integration. Add examples for Flask / WTForms???
